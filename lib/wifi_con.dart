import 'dart:async';

import 'package:flutter/services.dart';
enum WifiState { error, success, already }
class WifiCon {
  static const MethodChannel _channel = const MethodChannel('wifi_con');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static shareMessage(String? name) async {
    await _channel.invokeMethod('share', '${name}');
  }

  static Future<WifiState?> connection(String? ssid, String? password) async {
    final Map<String, dynamic> params = {
      'ssid': ssid,
      'password': password,
    };
    int? state = await _channel.invokeMethod('connection', params);
    switch (state) {
      case 0:
        return WifiState.error;
      case 1:
        return WifiState.success;
      case 2:
        return WifiState.already;
      default:
        return WifiState.error;
    }
  }
}
