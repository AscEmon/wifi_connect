#import "WifiConPlugin.h"
#if __has_include(<wifi_con/wifi_con-Swift.h>)
#import <wifi_con/wifi_con-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "wifi_con-Swift.h"
#endif

@implementation WifiConPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftWifiConPlugin registerWithRegistrar:registrar];
}
@end
