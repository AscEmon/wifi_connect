package com.example.wifi_con;

//from wifi
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

//default
import androidx.annotation.NonNull;
import android.util.Log;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;

/** WifiConPlugin */
public class WifiConPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
   private WifiDelegate delegate;
  // private final Registrar registrar;
   


 
//       private  Registrar registrar;
    // private WifiConPlugin(io.flutter.plugin.common.PluginRegistry.Registrar registrar,WifiDelegate delegate) {
    // this.registrar = registrar;
    //     this.delegate = delegate;
    // }

    /** Plugin registration. */
  @SuppressWarnings("deprecation")
  public static void registerWith(io.flutter.plugin.common.PluginRegistry.Registrar registrar) {
    final WifiConPlugin instance = new WifiConPlugin();
    instance.registersWith(registrar);
  }

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "wifi_con");
  //  registersWith(flutterPluginBinding);
    channel.setMethodCallHandler(this);
  }
  
  public static void registersWith(io.flutter.plugin.common.PluginRegistry.Registrar registrar) {
    
        
        WifiManager wifiManager = (WifiManager) registrar.activeContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        final WifiDelegate delegate = new WifiDelegate(registrar.activity(), wifiManager);
        registrar.addRequestPermissionsResultListener(delegate);

        // support Android O,listen network disconnect event
        // https://stackoverflow.com/questions/50462987/android-o-wifimanager-enablenetwork-cannot-work
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registrar
                .context()
                .registerReceiver(delegate.networkReceiver,filter);

        // channel.setMethodCallHandler(this);
    }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    }
    // else if(call.method.equals("share"))
    // {
    //  Log.d("TAG", "onMethodCall: " + call.arguments.toString());
    // } 
    // if (registrar.activity() == null) {
    //           result.error("no_activity", "wifi plugin requires a foreground activity.", null);
    //           return;
    //       }
    switch (call.method) {
      case "ssid":
          delegate.getSSID(call, result);
          break;
      case "level":
          delegate.getLevel(call, result);
          break;
      case "ip":
          delegate.getIP(call, result);
          break;
      case "list":
          delegate.getWifiList(call, result);
          break;
      case "connection":
      Log.d("TAG2", "connection: " + call.arguments.toString());
          delegate.connection(call, result);
          break;
      default:
          result.notImplemented();
          break;
  }
    // else if(call.method.equals("connection"))
    // {
    //   Log.d("TAG2", "connection: " + call.arguments.toString());
    // //    if (registrar.activity() == null) {
    // //         result.error("no_activity", "wifi plugin requires a foreground activity.", null);
    // //         return;
    // //     }
        
    // }
    
    
  }


  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  
// @Override
//   public void onAttachedToActivity(ActivityPluginBinding activityPluginBinding) {
//     // TODO: your plugin is now attached to an Activity
//     this.activity=activityPluginBinding.getActivity();
//   }

//   @Override
//   public void onDetachedFromActivityForConfigChanges() {
//     // TODO: the Activity your plugin was attached to was destroyed to change configuration.
//     // This call will be followed by onReattachedToActivityForConfigChanges().
//   }

//   @Override
//   public void onReattachedToActivityForConfigChanges(ActivityPluginBinding activityPluginBinding) {
//     // TODO: your plugin is now attached to a new Activity after a configuration change.
//   }

//   @Override
//   public void onDetachedFromActivity() {
//     // TODO: your plugin is no longer associated with an Activity. Clean up references.
//   }
}



// import android.content.Context;
// import android.content.IntentFilter;
// import android.net.ConnectivityManager;
// import android.net.wifi.WifiManager;

// import io.flutter.plugin.common.MethodCall;
// import io.flutter.plugin.common.MethodChannel;
// import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
// import io.flutter.plugin.common.MethodChannel.Result;
// import io.flutter.plugin.common.PluginRegistry.Registrar;

// public class WifiConPlugin implements MethodCallHandler {
//     private final Registrar registrar;
//     private WifiDelegate delegate;
//     private WifiConPlugin(Registrar registrar, WifiDelegate delegate) {
//         this.registrar = registrar;
//         this.delegate = delegate;
//     }

//     public static void registerWith(Registrar registrar) {
//         final MethodChannel channel = new MethodChannel(registrar.messenger(), "wifi_con");
//         WifiManager wifiManager = (WifiManager) registrar.activeContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//         final WifiDelegate delegate = new WifiDelegate(registrar.activity(), wifiManager);
//         registrar.addRequestPermissionsResultListener(delegate);

//         // support Android O,listen network disconnect event
//         // https://stackoverflow.com/questions/50462987/android-o-wifimanager-enablenetwork-cannot-work
//         IntentFilter filter = new IntentFilter();
//         filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
//         filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
//         registrar
//                 .context()
//                 .registerReceiver(delegate.networkReceiver,filter);

//         channel.setMethodCallHandler(new WifiConPlugin(registrar, delegate));
//     }

//     @Override
//     public void onMethodCall(MethodCall call, Result result) {
//         if (registrar.activity() == null) {
//             result.error("no_activity", "wifi plugin requires a foreground activity.", null);
//             return;
//         }
//         switch (call.method) {
//             case "ssid":
//                 delegate.getSSID(call, result);
//                 break;
//             case "level":
//                 delegate.getLevel(call, result);
//                 break;
//             case "ip":
//                 delegate.getIP(call, result);
//                 break;
//             case "list":
//                 delegate.getWifiList(call, result);
//                 break;
//             case "connection":
//                 delegate.connection(call, result);
//                 break;
//             default:
//                 result.notImplemented();
//                 break;
//         }
//     }

// }
